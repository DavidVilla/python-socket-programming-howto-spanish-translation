
MKHOWTO=/usr/lib/python2.7/doc/tools/mkhowto

HTML=$(patsubst %.tex, %.html, $(wildcard *.tex))
PDF= $(patsubst %.tex, %.pdf,  $(wildcard *.tex))

all: $(HTML) $(PDF)
	echo $(PDF)

%.pdf: %.tex
	$(MKHOWTO) --pdf $<

%.html: %.tex
	$(MKHOWTO) --html -s 0 $<

clean:
	$(RM) $(PDF) *~
	$(RM) -r $(basename  $(PDF))
